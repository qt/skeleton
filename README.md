# Project skeleton

This is a skeleton for research projects in the [Quantum Tinkerer](https://quantumtinkerer.tudelft.nl/) group at TU Delft.
It sets out a common directory structure between projects, and automatically configures things like continuous integration.

Anyone is free to use this skeleton (thanks to its CC0 license!), however if you are not a member of the QT group at TU Delft, your mileage may vary (at the very least you will need to tweak the setup script).

## Creating a new project

1. Get [pixi](https://pixi.sh) by following the installation instructions on the website.
2. Run `pixi exec copier copy https://gitlab.kwant-project.org/qt/skeleton.git --trust -r main <project_folder>`.
3. Answer the questions.
