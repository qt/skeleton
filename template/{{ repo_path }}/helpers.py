# Reusable and general purpose utility functions

from pathlib import Path
from time import time
import subprocess
import argparse
import zipfile


def execute_folder(folder_path=None):
    """Execute all python files in a folder."""
    if folder_path is None:
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "folder_path", help="Path to the folder containing the python files."
        )
        args = parser.parse_args()
        folder_path = args.folder_path

    folder = Path(folder_path)
    for file in folder.iterdir():
        if file.suffix != ".py":
            continue
        t = time()
        print(f"Executing {file.name}...")
        try:
            subprocess.run(
                ["python", file.relative_to(folder)], cwd=folder_path, check=True
            )
        except subprocess.CalledProcessError as e:
            print(f"Error executing {file.name}: {e}")
        print(f"Execution time: {time() - t:.2f}s\n")


def generate_zips():
    """Generate zips of everything relevant for Zenodo and arXiv submissions."""

    # Create arXiv zip
    arxiv_zip = zipfile.ZipFile("arxiv.zip", "w")
    for file in Path("publication").iterdir():
        if file.suffix in [".tex", ".bbl", ".cls", ".sty", ".bst"]:
            arxiv_zip.write(file)
    for file in Path("publication/figures").iterdir():
        if file.suffix in [".svg"]:  # Raw figure, skip
            continue
        if file.name == ".gitkeep":
            continue
        arxiv_zip.write(file)
    arxiv_zip.close()

    # Create Zenodo zip
    zenodo_zip = zipfile.ZipFile("zenodo.zip", "w", compression=zipfile.ZIP_STORED)
    base = Path(".")
    children = [
        "simulations",
        "src_figures",
        "data",
        "results",
        "publication/figures/.gitkeep",
        "pixi.lock",
        "pyproject.toml",
    ]
    for child in children:
        if (base / child).is_file():
            zenodo_zip.write(base / child)
        else:
            for file in (base / child).rglob("**"):
                zenodo_zip.write(file)
    zenodo_zip.close()
    print("Zips generated.")
